class Cat:
    def __init__(self, cat):
        self.cat = cat
    """ Магический метод предопределяющий вывод инфы"""
    def __repr__(self):
        return f"{self.__class__}: {self.cat}"

    """ Отличия: 
    __repr__ больше для отладки, и нужен разработчикам
    __str__  , больше для пользователей, и нужен для функции print"""

    def __str__(self):
        return f"{self.cat}"


print("-------------------------\n::Example 1, __repr__")
example_1 = Cat("Vasaya")
print(example_1)


class Point:
    def __init__(self, *args):
        self.__coords = args
    """ Магический метод len , позволяет применять len к подсчету длины свойств класса, и возвращать длину"""
    def __len__(self):
        return len(self.__coords)

    """ Магический метод который вернет модули всех чисел в списке"""
    def __abs__(self):
        return list(map(abs, self.__coords))


print("_______________________\n::Example #2 , __len__")
example_2 = Point(1, 2, 3, 4)
print(len(example_2))

print("________________________\n::Example #3 , __abs__")
example_3 = Point(-12, -2, 0, 3)
print(abs(example_3))