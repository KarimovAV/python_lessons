class Clock:
    __DAY = 86400

    def __init__(self, seconds: int):
        if not isinstance(seconds, int):
            raise TypeError("Секунды должны быть целым числом")
        self.seconds = seconds % self.__DAY

    def get_time(self):
        s = self.seconds % 60
        m = (self.seconds // 60) % 60
        h = (self.seconds // 3600) % 24
        return f"{self.__get_formatted(h)}:{self.__get_formatted(m)}:{self.__get_formatted(s)}"

    @classmethod
    def __get_formatted(cls, x):
        return str(x).rjust(2, "0")

    """ Метод add позволяет выполнять операции через + , просто указывая экземпляр класса"""
    def __add__(self, other):
        if not isinstance(other, (int, Clock)):
            raise ArithmeticError("Правый операнд должен быть целым числом или объектом Clock")

        sc = other
        if isinstance(sc, Clock):
            sc = other.seconds

        return Clock(self.seconds + sc)

    """ Метод дополняющий метод __add, для тех случаев, если к примеру не должен быть важен порядок суммирования
    экземпляров классов, иначе примеры 100 + Object, Object + 100 будут иметь разный эффект"""
    def __radd__(self, other):
        return self + other

    """ Метод дополняющий __add, позволяющий использовать сокращенные += """
    def __iadd__(self, other):
        if not isinstance(other, (int, Clock)):
            raise ArithmeticError("Правый операнд должен быть типом int или объектом Clock")

        sc = other
        if isinstance(other, Clock):
            sc = other.seconds

        self.seconds += sc
        return self


example_1 = Clock(seconds=1000)
print(f"______________________\nДо __add:  {example_1.get_time()}\n")
example_1 = example_1 + 200
print(f"После __add: {example_1.get_time()}\n")

"""" Пример реализации, когда через add можно суммировать свойства разных экземпляров класса """
example_2 = Clock(seconds=500)
example_3 = Clock(seconds=2300)
example_4 = example_2 + example_3
print(f"--------------------\nПример суммирования разных экземпляров:\n{example_4.get_time()}")