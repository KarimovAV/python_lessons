class Point:
    def __init__(self, a: int, b: int):
        self.a = a
        self.b = b

    """ Метод который предопределяет логику функции bool в отношении экземпляров класса.
    Мы поменяли логику, теперь будет возвращаться True при равенстве двух свойств"""
    def __bool__(self):
        print("__bool__")
        return self.a == self.b


example_one = Point(a=10, b=10)
print(bool(example_one))