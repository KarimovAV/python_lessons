"""
Полиморфизм помогает делать единый интерфейс дял взаимодействия с объектами
В примере у нас будет три класса с одинаковым методом для подсчета периметра
"""


class Geom:
    """
    Здесь данный метод можно назвать абстрактным, он нужен для того, чтобы вызывать исключение, если в дочернем классе
    не был предопределен метод get_pr
    """
    def get_pr(self):
        raise NotImplementedError("В каждом дочернем классе должен быть предопределен get_pr")


class Rectangle(Geom):
    def __init__(self, w, h):
        self.w = w
        self.h = h

    def get_pr(self):
        return 2 * (self.w + self.h)


class Square(Geom):
    def __init__(self, a):
        self.a = a

    def get_pr(self):
        return 4 * self.a


class Triangle(Geom):
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def get_pr(self):
        return self.a + self.b + self.c


r1 = Rectangle(2, 3)
r2 = Rectangle(3, 2)
s1 = Square(3)
s2 = Square(4)
t1 = Triangle(3, 4, 5)
t2 = Triangle(1, 2, 3)
list_objects = [r1, r2, s1, s2, t1, t2]

for object_perimetr in list_objects:
    print(object_perimetr.get_pr())


