class Point:
    """ cls ссылается на сам класс"""
    def __new__(cls, *args, **kwargs):
        print("вызов __new__ для " + str(cls))

        """ возвращает ссылку на создаваемый экземпляр класса, таким образом сперва выполнится метод __new__
        а после __init__ у создаваемого экземпляра класса"""
        return super().__new__(cls)

    """" self ссылается на экземпляр класса"""
    def __init__(self, x=0, y=0):
        print("вызов __init__ для " + str(self))
        self.x = x
        self.y = y


""" Пример паттерна Signleton, в котором может быть только один экземпляр класса
                При помощи __new__, это можно будет сделать"""


class DataBase:
    __instance = None

    def __new__(cls, *args, **kwargs):
        if cls.__instance is None:
            cls.__instance = super().__new__(cls)
        return cls.__instance

    def __del__(self):
        DataBase.__instance = None

    def __init__(self, user, psw, port):
        self.user = user
        self.psw = psw
        self.port = port

    def connect(self):
        print(f'Соединение с БД, USER: {self.user}')

    def close(self):
        print(f'Разъединени с БД, USER: {self.user}')