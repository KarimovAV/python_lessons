class LessonTwo:
    MIN_NUM = 10
    MAX_NUM = 100

    """ classmethod позволяет обращатся к методу класса без self"""
    @classmethod
    def validate(cls, arg):
        return cls.MIN_NUM <= arg <= cls.MAX_NUM

    def __init__(self, x, y):
        self.x = self.y = 0
        if LessonTwo.validate(x) and LessonTwo.validate(y):
            self.x = self.x
            self.y = self.y