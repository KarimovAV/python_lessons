class ExampleProtected:
    """-x, _y - так помечаются свойства в режиме Protected. Они показывают, что эти свойства трогать не надо,
    но в практическом плане, не защищает свойства от обращений извне"""
    def __init__(self, x=0, y=0):
        self._x = x
        self._y = y


a = ExampleProtected()
print(a._x)
print(a._y)


class ExamplePrivate:
    """" пометка __ даёт атрибуту Private режим, извне обращаться не получится, но можно обращаться внутри класс при
    помощи методов"""
    def __init__(self, x=0, y=20):
        self.__x = x
        self.__y = y

    """ свойством Private также можно наделить метод класса"""
    @classmethod
    def __check_value(cls, x):
        return type(x) in (int, float)

    def set_attribute(self, x_x, y_y):
        if self.__check_value(x_x) and self.__check_value(y_y):
            self.__x = x_x
            self.__y = y_y
        else:
            raise ValueError("Значения должны быть типа int иди float")

    def get_private_at(self):
        return self.__x, self.__y


private_class = ExamplePrivate()
print(private_class.get_private_at())

private_class.set_attribute(10, 200)
print(private_class.get_private_at())

"""Тем не менее Private не защищает полностью свойства от обращения извне, при помощи dir можно посмотреть кодовое 
имя свойства, и обратится по нему"""
print(dir(private_class))
print(private_class._ExamplePrivate__x)

private_class._ExamplePrivate__x = 1100
print(private_class.get_private_at())
