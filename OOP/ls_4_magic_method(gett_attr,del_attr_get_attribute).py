class PointGetAttribute:
    def __init__(self, x=10, y=20):
        self.x = x
        self.y = y

    """ Метод будет вызываться во всех случаях, когда переменным присваивается атрибут экземпляра класс"""
    def __getattribute__(self, item):
        print("__getattribute__")
        return object.__getattribute__(self, item)

print("----------------\nExample 1")
example_get = PointGetAttribute()
a = example_get.x


class PointGetAttributeTwo:
    def __init__(self, x=10, y=20):
        self.x = x
        self.y = y

    """ Пример, это можно использовать чтобы ограничить доступ к атрибуту класса"""
    def __getattribute__(self, item):
        if item == "x":
            raise ValueError("Доступ к x запрещен")
        else:
            return object.__getattribute__(self, item)


print("----------------\nExample 2")
# example_2 = PointGetAttributeTwo()
# a_2 =example_2.x


class PointSetAttribute:
    def __init__(self, x=10, y=20):
        self.x = x
        self.y = y

    """ Магический метод. Будет вызываться каждый раз когда происходит инициализация / 
        создания свойства у экземпляра класса. В примере мы запрещаем создавать свойство z"""
    def __setattr__(self, key, value):
        if key == "z":
            raise ValueError("атрибут недоступен")
        else:
            object.__setattr__(self, key, value)


example_3 = PointSetAttribute()
example_3.z = 100


class PointDelGet:
    def __init__(self, x=10, y=20):
        self.x = x
        self.y = y

    """Магический метод который будет возвращаться при обращение к несуществующему атрибуту"""
    def __getattr__(self, item):
        return False

    """Магический метод который будет возвращаться при удалении """
    def __delattr__(self, item):
        print(f"__delattr__: {item}")
        return object.__delattr__(self, item)


