class ThreadClass:
    """Паттерн Моносостояния. Позволяет задать свойства общие для всех создаваемых экземпляров класса.
    При изменении этих свойств, они будут меняться для всех созданных экземпляров"""

    __shared_attrs = {
        'name': 'thread 1',
        'dict': {},
        'id': 10
    }

    def __init__(self):
        self.__dict__ = self.__shared_attrs


thread_1 = ThreadClass()
thread_2 = ThreadClass()

print(f'dict экземпляров класса:\n {thread_1.__dict__}\n{thread_2.__dict__}\n')

thread_1.id = -100
print(f'dict экземпляров класса после изменений:\n {thread_1.__dict__}\n{thread_2.__dict__}')
