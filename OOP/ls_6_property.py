class Person:
    def __init__(self, name, old):
        self.__name = name
        self.__old = old

    def get_old(self):
        return self.__old

    def set_old(self, old):
        self.__old = old

    """ При помощи property мы теперь можем обращаться к методам класса посредством атрибута old"""
    old = property(get_old, set_old)


""" Пример"""
example = Person(name="Sergey", old=40)
""" Таким образом обращаемся с методу get_old через property """
a = example.old
print(a)
"Таким образом обращаемся к методу set_old"
example.old = 100
print(example.__dict__)


class Person_2:
    """ В данном классе указано альтернативное property, оно будет выполнять те же функции что и в Person_1"""
    def __init__(self, name, old):
        self.__name = name
        self.__old = old

    @property
    def old(self):
        return self.__old

    @old.setter
    def old(self, old):
        self.__old = old

    """ ++ добавили deleter для old, теперь при вызове del атрибут old будет удаляться"""
    @old.deleter
    def old(self):
        del self.__old

example_2 = Person_2(name='Svetlan', old=120)
print(example_2.old)
example_2.old = 130
print(example_2.old)

""" Пример удаления -> удалям -> смотрим через dict что атрибут был удален"""
del example_2.old
print(example_2.__dict__)