class Integer:
    """ Класс дескриптер который будет использоваться в других классах, при инициализации свойств, будут
    применяться методы внутри данного класса"""
    def __set_name__(self, owner, name):
        self.name = "_" + name

    def __get__(self, instance, owner):
        return instance.__dict__[self.name]

    def __set__(self, instance, value):
        print(f"__set__:{self.name}={value}")
        instance.__dict__[self.name]=value


class Point3D:
    x = Integer()
    y = Integer()
    z = Integer()

    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    @classmethod
    def verify_coord(cls, coord):
        if type(coord) != int:
            raise TypeError("Координата должна быть числом")


pt = Point3D(1, 2, 3)
print(pt.__dict__)