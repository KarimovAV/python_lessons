from string import ascii_letters


class Person:
    S_RUS = 'йцукенгшщзхъфывапролджэячсмитьбю-'
    S_RUS_UPPER = S_RUS.upper()

    def __init__(self, fio, old, ps, weight):
        self.verify_fio(fio)
        self.verify_old(old)
        self.verify_passport(ps)
        self.verify_weight(weight)

        self.__fio = fio.split()
        self.__old = old
        self.__ps = ps
        self.__weight = weight

    """ Сперва для каждого свойства задаем метод класса для верификации свойства"""

    @classmethod
    def verify_fio(cls, fio):
        if type(fio) != str:
            raise TypeError("ФИО должно быть строкой")

        f = fio.split()
        if len(f) != 3:
            raise TypeError("Формат ФИО некорректен")

        letters = ascii_letters + cls.S_RUS + cls.S_RUS_UPPER
        for s in f:
            if len(s) < 1:
                raise TypeError("В ФИО должен быть минимум один символ")
            if len(s.strip(letters)) != 0:
                raise ("В ФИО присутствуют некорректные символы")

    @classmethod
    def verify_old(cls, old):
        if type(old) != int or old < 14 or old > 120:
            raise TypeError("Возраст не подходит по формату")

    @classmethod
    def verify_weight(cls, w):
        if type(w) != float or w < 20:
            raise TypeError("Вес должен быть больше 20 и float")

    @classmethod
    def verify_passport(cls, p):
        if type(p) != str:
            raise TypeError("Тип данных должен быть строкой")

        s = p.split()
        if len(s) != 2 or len(s[0]) != 4 or len(s[1]) != 6:
            raise TypeError("Неверный формат паспорта")

        for p in s:
            if not p.isdigit():
                raise TypeError("Значения в серии и номере паспортса должны быть цифрами")

    """ Теперь зададим property для интерфейса чтобы взаимодействовать с свойствами"""

    @property
    def fio(self):
        return self.__fio

    @property
    def old(self):
        return self.__old
    @old.setter
    def set_old(self, old):
        self.verify_old(old)
        self.__old = old

    @property
    def weight(self):
        return self.__weight
    @weight.setter
    def set_weight(self, weight):
        self.verify_weight(weight)
        self.__weight = weight

    @property
    def passport(self):
        return self.__ps
    @passport.setter
    def set_passport(self, ps):
        self.verify_passport(ps)
        self.__ps = ps


p = Person(fio="Антон Каримов Витальевич",
           old=28,
           ps='1234 11231',
           weight=86)
