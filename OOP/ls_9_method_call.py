import math


class Counter:
    """ В данном классе мы устанавливаем счетчик, при помощи метода call, этот счетчки будет увеличиваться при каждом вызове
    экземпляра класса"""

    def __init__(self):
        self.__counter = 0

    def __call__(self, *args, **kwargs):
        print("__call__")
        self.__counter += 1
        return self.__counter


example = Counter()
example()
example()

a = example()
print(a)


class StripChars:
    """Здесь при помощи call мы сможем применять strip к значениям которые будут прокидываться при вызове класс"""
    def __init__(self, chars):
        self.__counter = 0
        self.__chars = chars

    def __call__(self, *args, **kwargs):
        if not isinstance(args[0], str):
            raise TypeError("Аргумент должен быть строкой")

        return args[0].strip(self.__chars)


check_a = StripChars('?:!.; "')
res = check_a("Hello World!")
print("Пример StripChars")
print(res)


class Derivate:
    """Пример класса декоратора. Метод call будет использоваться для приименения к функции, чтобы добавить
    дополнительный расчет"""
    def __init__(self, func):
        self.__fn = func

    def __call__(self, x, dx=0.0001, *args, **kwargs):
        return (self.__fn(x + dx) - self.__fn(x)) / dx


""" Создается функция с значением x которая считае синус. При помощи декоратора класса мы также расчитаме производную
после поулчения синуса x"""
@Derivate
def df_sin(x):
    return math.sin(x)


print(df_sin(math.pi/3))